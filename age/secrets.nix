let
  hosts = {
    moonrock = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAo6XicELXQKavcCAC1HzJ6IEv7P5nEcCKWm/zs0u7Iw";
    gatekeeper = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAodkNf09Jig60sjYJDv0EpaPpEvd+nOWzs6Rn1ZV1eV";
    right-turn = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINW6TAMZ2VXT4K08r0HApgX7OPne2XSpX6a6kLn0vLCz";
    concourse-web = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMjKkvmYnMRmCJRGIPnzQmjJEmWg+QbGQUJCaRzFMeoQ";
  };
  users = {
    axeman = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMNeQYLFauAbzDyIbKC86NUh9yZfiyBm/BtIdkcpZnSU";
    morino = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFw0M6sSKJzhbV5xWbFOtNYsTe04xDC+O0CqqkKKTLRu";
  };
  ratholeKeys = [users.axeman hosts.moonrock hosts.gatekeeper];
in {
  "rathole-moonrock-noise-private-key.age".publicKeys = [users.axeman hosts.moonrock];
  "rathole-gatekeeper-noise-private-key.age".publicKeys = [users.axeman hosts.gatekeeper];

  "rathole-moonrock-grafana-token.age".publicKeys = ratholeKeys;
  "rathole-moonrock-nginx-plain-token.age".publicKeys = ratholeKeys;
  "rathole-moonrock-nginx-tls-token.age".publicKeys = ratholeKeys;

  "nextcloud-admin-pass.age".publicKeys = [users.axeman hosts.moonrock];

  "berlin-scraper-telegram-bot-token.age".publicKeys = [users.morino users.axeman hosts.gatekeeper];

  "openvpn-moonrock-boot-key.age".publicKeys = [users.axeman hosts.gatekeeper hosts.moonrock];

  "home-wifi-psk.age".publicKeys = [users.axeman hosts.right-turn];

  "concourse-tsa-host-key.age".publicKeys = [users.axeman hosts.concourse-web];
  "concourse-session-signing-key.age".publicKeys = [users.axeman hosts.concourse-web];
  "concourse-worker-1-tsa-key.age".publicKeys = [users.axeman];
}
