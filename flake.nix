{
  description = "NixOS and home-manager config";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    nixos_stable.url = "github:NixOS/nixpkgs/nixos-24.11";
    nixos_old.url = "github:NixOS/nixpkgs/nixos-23.05";

    flake-utils.url = "github:numtide/flake-utils";

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    home-manager-stable = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixos_stable";
    };

    emacs-overlay = {
      url = "github:nix-community/emacs-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };

    deploy-rs = {
      url = "github:serokell/deploy-rs";
      inputs.utils.follows = "flake-utils";
    };

    agenix.url = "github:ryantm/agenix";

    terraform-http-backend-pass = {
      url = "git+https://git.coop/akshay/terraform-http-backend-pass";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };

    wohnungs-suche = {
      url = "git+https://git.pub.solar/axeman/wohnugs-suche.git?ref=wbs";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };

    concourse = {
      url = "git+https://codeberg.org/axeman/concourse-flake";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
  };
  outputs = inputs@{nixpkgs, home-manager, home-manager-stable, emacs-overlay, nixos_stable, deploy-rs, terraform-http-backend-pass, agenix, wohnungs-suche, ...}: rec {
    pkgs = import nixpkgs {
      system = "x86_64-linux";
      overlays = [
        emacs-overlay.overlay
        (import ./pkgs/overlay.nix)
      ];
      config.allowUnfree = true;
    };
    pkgs-aarch64 = import nixpkgs {
      system = "aarch64-linux";
      overlays = [
        (import ./pkgs/overlay.nix)
        inputs.concourse.overlays.aarch64-linux.default
      ];
    };
    gnucash =
      let oldpkgs = import inputs.nixos_old {
            system = "x86_64-linux";
          };
      in oldpkgs.gnucash;
    stable = import nixos_stable {
      system = "x86_64-linux";
      config.allowUnfree = true;
      overlays = [
        (import ./pkgs/overlay.nix)
        (self: super: {berlin-scraper = wohnungs-suche.packages.aarch64-linux.berlin-scraper;})
      ];
    };
    stable-aarch64 = import nixos_stable {
      system = "aarch64-linux";
      config.allowUnfree = true;
      overlays = [
        (import ./pkgs/overlay.nix)
        (self: super: {berlin-scraper = wohnungs-suche.packages.aarch64-linux.berlin-scraper;})
      ];
    };
    deployPkgs = import nixos_stable {
      system = "x86_64-linux";
      overlays = [
        deploy-rs.overlay
        (self: super: { deploy-rs = { inherit (pkgs) deploy-rs; lib = super.deploy-rs.lib; }; })
      ];
    };

    deployPkgs-aarch64 = import nixos_stable {
      system = "aarch64-linux";
      overlays = [
        deploy-rs.overlay
        (self: super: { deploy-rs = { inherit (stable-aarch64) deploy-rs; lib = super.deploy-rs.lib; }; })
      ];
    };

    devEnv = pkgs.buildEnv {
      name = "nixos-config-dev-env";
      paths = [
        deployPkgs.deploy-rs.deploy-rs
        pkgs.gnumake
        pkgs.jq
        pkgs.parted
        # (pkgs.haskell.lib.justStaticExecutables pkgs.haskellPackages.terraform-http-backend-pass)
        terraform-http-backend-pass.outputs.packages.x86_64-linux.terraform-http-backend-pass-static
        (pkgs.terraform.withPlugins(p: with p; [oci hcloud gandi pass external]))
        agenix.packages.x86_64-linux.default
        pkgs.openvpn
      ];
    };

    homeConfigurations = {
      "axeman@tuxnix" = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;
        modules = [
          ./hosts/tuxnix/axeman/home.nix
          {
            home = {
              username = "axeman";
              homeDirectory = "/home/axeman";
              stateVersion = "22.05";
            };
          }
        ];
      };
      "axeman@wireman-axe" = home-manager-stable.lib.homeManagerConfiguration {
        pkgs = stable;
        modules = [
          ./hosts/wireman-axe/axeman/home.nix
          {
            home = {
              username = "axeman";
              homeDirectory = "/home/axeman";
              stateVersion = "21.03";
            };
          }
        ];
        extraSpecialArgs = {
          unstable = pkgs;
          home-manager-unstable = home-manager;
        };
      };
    };

    nixosConfigurations = {
      tuxnix = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [./hosts/tuxnix/configuration.nix];
      };

      wireman-axe = nixos_stable.lib.nixosSystem {
        system = "x86_64-linux";
        specialArgs = { personalLaptop = false; };
        modules = [./hosts/wireman-axe/configuration.nix];
      };

      # sudo nixos-rebuild switch -p personal-laptop --flake '/home/axeman/workspace/nixos-config#wireman-axe-personal'
      wireman-axe-personal = nixos_stable.lib.nixosSystem {
        system = "x86_64-linux";
        specialArgs = { personalLaptop = true; };
        modules = [./hosts/wireman-axe/configuration.nix];
      };

      right-turn = nixos_stable.lib.nixosSystem {
        pkgs = stable-aarch64;
        modules = [
          ./hosts/right-turn/configuration.nix
          agenix.nixosModules.default
        ];
      };

      moonrock = nixos_stable.lib.nixosSystem {
        pkgs = stable-aarch64;
        modules = [
          ./hosts/moonrock/configuration.nix
          agenix.nixosModules.default
        ];
      };

      gatekeeper = nixos_stable.lib.nixosSystem {
        pkgs = stable-aarch64;
        system = "aarch64-linux";
        modules = [
          ./hosts/gatekeeper/configuration.nix
          agenix.nixosModules.default
        ];
      };

      concourse-web = nixpkgs.lib.nixosSystem {
          pkgs = pkgs-aarch64;
          system = "aarch64-linux";
          modules = [
            # ({... }: { nixpkgs.overlays = [ inputs.concourse.overlays.x86_64-linux.default ]; })
            ./hosts/concourse-web/configuration.nix
            agenix.nixosModules.default
            inputs.concourse.nixosModules.concourse-web
          ];
        };
    };

    deploy.nodes = {
      right-turn = {
        hostname = "192.168.178.38";
        profiles.system = {
          sshUser = "root";
          user = "root";
          path = deployPkgs-aarch64.deploy-rs.lib.activate.nixos nixosConfigurations.right-turn;
        };
      };

      moonrock = {
        hostname = "moonrock";
        profiles.system = {
          sshUser = "root";
          user = "root";
          path = deployPkgs-aarch64.deploy-rs.lib.activate.nixos nixosConfigurations.moonrock;
        };
      };

      gatekeeper = {
        hostname = "gatekeeper.bytes.gdn";
        profiles.system = {
          sshUser = "root";
          user = "root";
          path = deployPkgs-aarch64.deploy-rs.lib.activate.nixos nixosConfigurations.gatekeeper;
        };
      };

      concourse-web = {
        hostname = "concourse.gdn";
        profiles.system = {
          sshUser = "root";
          user = "root";
          path = deployPkgs-aarch64.deploy-rs.lib.activate.nixos nixosConfigurations.concourse-web;
        };
      };
    };

    checks = builtins.mapAttrs (system: deployLib: deployLib.deployChecks deploy) deploy-rs.lib;
  };
}
