
TFSTATE_BACKEND_PORT := 8888
TFSTATE_REPO_PATH := $(CURDIR)/terraform/.tfstate
TFSTATE_REPO := git@git.coop:akshay/tfstate.git

## State backend

$(TFSTATE_REPO_PATH):
	git clone $(TFSTATE_REPO) $(TFSTATE_REPO_PATH)

.PHONY: run-tfstate-backend
run-tfstate-backend: $(TFSTATE_REPO_PATH)
	terraform-http-backend-pass --port "$(TFSTATE_BACKEND_PORT)" --repositoryPath "$(TFSTATE_REPO_PATH)"

.PHONY: start-tfstate-backend
start-tfstate-backend: $(TFSTATE_REPO_PATH)
	nohup terraform-http-backend-pass --port "$(TFSTATE_BACKEND_PORT)" --repositoryPath "$(TFSTATE_REPO_PATH)" 2>&1 > tfstate-backend.log & echo $$! > tfstate-backend.pid

.PHONY: stop-tfstate-backend
stop-tfstate-backend:
	kill $(shell cat tfstate-backend.pid) && rm tfstate-backend.pid

.PHONY: force-stop-tfstate-backend
force-stop-tfstate-backend:
	kill -9 $(shell cat tfstate-backend.pid) && rm tfstate-backend.pid
