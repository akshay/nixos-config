{pkgs, lib, ...}:
let
  config = builtins.readFile ./51-bluez-config.lua;
in if (builtins.compareVersions lib.version "23.11" > 0)
   then {
     services.pipewire.wireplumber.configPackages = [
       (pkgs.writeTextDir "share/wireplumber/bluetooth.lua.d/51-bluez-config.lua" config)
     ];
   }
   else {
     environment.etc = {
       "wireplumber/bluetooth.lua.d/51-bluez-config.lua".text = config;
     };
   }
