{ pkgs,lib, ... }:
if (builtins.compareVersions lib.version "23.11" > 0)
then {
  services.pipewire.extraConfig.pipewire."50-combine-sinks.conf" = {
    context.modules = [{
      name = "libpipewire-module-combine-stream";
      args = {
        combine.mode = "sink";
        node.name = "combine_sink";
        node.description = "Send audio everywhere";
        combine.props = {
          audio.position = [ "FL" "FR" ];
        };
        stream.props = {
          node.name = "SplitAudio";
        };
        stream.rules = [{
          matches = [
            # any of the items in matches needs to match, if one does,
            # actions are emited.
            {
              # all keys must match the value. ~ in value starts regex.
              #node.name = "~alsa_input.*"
              media.class = "Audio/Sink";
            }
          ];
          actions = {
            create-stream = {
              #combine.audio.position = [ FL FR ]
              #audio.position = [ FL FR ]
            };
          };
        }];
      };
    }];
  };
}
else {
  environment.etc = {
    "pipewire/pipewire.conf.d/50-combine-sinks.conf".text = builtins.readFile ./50-combine-sinks.conf;
  };
}
