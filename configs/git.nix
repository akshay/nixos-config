{
  enable = true;
  userName = "Akshay Mankar";
  userEmail = "itsakshaymankar@gmail.com";
  signing.key = "30E3663D6E58E10DB1FFA9E7CA08F3AB62369B89";
  signing.signByDefault = true;
  aliases = {
    co = "checkout";
    st = "status";
    lol = "log --oneline --all --abbrev-commit --graph --decorate --color";
    git = "!exec git";
  };
  extraConfig = {
    init.defaultBranch = "main";
    rebase.autoStash = true;
    pull.rebase = true;
    rerere.enabled = true;
    merge.conflictstyle = "zdiff3";
  };
  ignores = [
    "*~"
    "*.swp"
    ".dir-local.el"
  ];
  diff-so-fancy.enable = true;
}
