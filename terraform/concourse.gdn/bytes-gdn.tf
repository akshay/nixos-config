data "gandi_livedns_domain" "bytes-gdn" {
  name = "bytes.gdn"
}

resource "gandi_livedns_record" "cloud" {
  zone = data.gandi_livedns_domain.bytes-gdn.id
  name = "cloud"
  type = "CNAME"
  ttl    = 300
  values = ["nx24411.your-storageshare.de."]
}
