terraform {
  backend "http" {
    address = "http://localhost:8888/state/concourse-gdn"
  }
  required_providers {
    hcloud = {
      source = "hetznercloud/hcloud"
    }
    pass = {
      source = "camptocamp/pass"
    }
    gandi = {
      source = "go-gandi/gandi"
    }
  }
  required_version = ">= 1.1.9"
}
