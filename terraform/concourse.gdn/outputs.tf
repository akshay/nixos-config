output "servers" {
  value = {
    web = {
      ipv4_address = hcloud_server.concourse-web.ipv4_address
      ipv6_address = hcloud_server.concourse-web.ipv6_address
      data_device  = hcloud_volume.concourse-web-data.linux_device
    },
    workers = { for name, server in hcloud_server.workers :
      name => {
        ipv4_address = server.ipv4_address
        ipv6_address = server.ipv6_address
      }
    }
  }
}
