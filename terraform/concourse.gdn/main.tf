provider "pass" {}

data "pass_password" "hcloud-token" {
  path = "hcloud-token-concourse"
}

data "pass_password" "gandi-api-key" {
  path = "gandi-api-key"
}

provider "hcloud" {
  token = data.pass_password.hcloud-token.password
}

provider "gandi" {
  key = data.pass_password.gandi-api-key.password
}

resource "hcloud_ssh_key" "operator" {
  name       = "operator"
  public_key = file("../../public-keys/concourse-operator.pem")
}

#######
# ATC #
#######

resource "hcloud_server" "concourse-web" {
  name        = "concourse-web"
  server_type = "cax11"
  image       = "ubuntu-20.04"
  ssh_keys    = [hcloud_ssh_key.operator.name]
  location    = "nbg1"
  iso         = "nixos-minimal-24.11.712431.cbd8ec4de446-aarch64-linux.iso"
}

resource "hcloud_volume" "concourse-web-data" {
  name      = "concourse-web-data"
  size      = 10
  server_id = hcloud_server.concourse-web.id
}

data "gandi_livedns_domain" "concourse-gdn" {
  name = "concourse.gdn"
}

resource "gandi_livedns_record" "ci-concourse-gdn-A" {
  zone   = data.gandi_livedns_domain.concourse-gdn.id
  name   = "@"
  type   = "A"
  ttl    = 300
  values = [hcloud_server.concourse-web.ipv4_address]
}

resource "gandi_livedns_record" "ci-concourse-gdn-AAAA" {
  zone   = data.gandi_livedns_domain.concourse-gdn.id
  name   = "@"
  type   = "AAAA"
  ttl    = 300
  values = [hcloud_server.concourse-web.ipv6_address]
}

###########
# WORKERS #
###########

resource "hcloud_server" "workers" {
  for_each = {
    worker-1 = { type = "cx21" }
  }

  name        = each.key
  server_type = each.value.type
  image       = "ubuntu-20.04"
  ssh_keys    = [hcloud_ssh_key.operator.name]
  user_data   = <<EOF
#cloud-config

runcmd:
- curl https://raw.githubusercontent.com/elitak/nixos-infect/master/nixos-infect | PROVIDER=hetznercloud NIX_CHANNEL=nixos-20.09 bash 2>&1 | tee /tmp/infect.log
EOF
}

resource "gandi_livedns_record" "workers-concourse-gdn-A" {
  for_each = hcloud_server.workers

  zone   = data.gandi_livedns_domain.concourse-gdn.id
  name   = each.key
  type   = "A"
  ttl    = 300
  values = [each.value.ipv4_address]
}

resource "gandi_livedns_record" "workers-concourse-gdn-AAAA" {
  for_each = hcloud_server.workers

  zone   = data.gandi_livedns_domain.concourse-gdn.id
  name   = each.key
  type   = "AAAA"
  ttl    = 300
  values = [each.value.ipv6_address]
}
