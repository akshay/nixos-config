provider "pass" {}

data "pass_password" "hcloud_token" {
  path = "hcloud-token"
}

data "pass_password" "gandi-api-key" {
  path = "gandi-api-key"
}

provider "hcloud" {
  token = data.pass_password.hcloud_token.password
}

provider "gandi" {
  key = data.pass_password.gandi-api-key.password
}

resource "hcloud_ssh_key" "axeman" {
  name       = "axeman"
  public_key = file("../../public-keys/axeman")
}

resource "hcloud_primary_ip" "v4" {
  name          = "gatekeeper-ipv4"
  datacenter    = "fsn1-dc14"
  type          = "ipv4"
  assignee_type = "server"
  auto_delete   = false
}

resource "hcloud_primary_ip" "v6" {
  name          = "gatekeeper-ipv6"
  datacenter    = "fsn1-dc14"
  type          = "ipv6"
  assignee_type = "server"
  auto_delete   = false
}

resource "hcloud_server" "gatekeeper" {
  name        = "gatekeeper"
  server_type = "cax11"
  location    = "fsn1"
  image       = "ubuntu-22.04"
  ssh_keys    = [hcloud_ssh_key.axeman.name]
  public_net {
    ipv4 = hcloud_primary_ip.v4.id
    ipv6 = hcloud_primary_ip.v6.id
  }
  user_data   = <<EOF
#cloud-config

runcmd:
- curl https://raw.githubusercontent.com/elitak/nixos-infect/master/nixos-infect | PROVIDER=hetznercloud NIX_CHANNEL=nixos-23.05 bash 2>&1 | tee /tmp/infect.log
EOF
}

data "gandi_livedns_domain" "bytes_gdn" {
  name = "bytes.gdn"
}

resource "gandi_livedns_record" "gatekeeper_bytes_gdn_A" {
  zone   = data.gandi_livedns_domain.bytes_gdn.id
  name   = "gatekeeper"
  type   = "A"
  ttl    = 300
  values = [hcloud_primary_ip.v4.ip_address]
}

resource "gandi_livedns_record" "gatekeeper_bytes_gdn_AAAA" {
  zone   = data.gandi_livedns_domain.bytes_gdn.id
  name   = "gatekeeper"
  type   = "AAAA"
  ttl    = 300
  values = [cidrhost(hcloud_primary_ip.v6.ip_network, 1)]
}

resource "gandi_livedns_record" "test_bytes_gdn_A" {
  zone   = data.gandi_livedns_domain.bytes_gdn.id
  name   = "test"
  type   = "A"
  ttl    = 300
  values = [hcloud_primary_ip.v4.ip_address]
}

resource "gandi_livedns_record" "test_bytes_gdn_AAAA" {
  zone   = data.gandi_livedns_domain.bytes_gdn.id
  name   = "test"
  type   = "AAAA"
  ttl    = 300
  values = [cidrhost(hcloud_primary_ip.v6.ip_network, 1)]
}
