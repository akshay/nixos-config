terraform {
  backend "http" {
    address = "http://localhost:8888/state/nixos-config-gatekeeper"
  }

  required_providers {
    pass = {
      source = "camptocamp/pass"
    }
    hcloud = {
      source = "hetznercloud/hcloud"
    }
    gandi = {
      source = "go-gandi/gandi"
    }
  }
}
