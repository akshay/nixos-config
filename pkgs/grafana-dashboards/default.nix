{fetchurl, writeTextFile}: {
  node-exporter-full =
    let file = fetchurl {
          url = "https://grafana.com/api/dashboards/1860/revisions/31/download";
          sha256 = "sha256-QsRHsnayYRRGc+2MfhaKGYpNdH02PesnR5b50MDzHIg=";
        };
     in writeTextFile {
       name = "node-exporter-full";
       text = (builtins.readFile file);
       destination = "/share/dashboard.json";
     };
}
