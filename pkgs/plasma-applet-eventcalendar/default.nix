{
  stdenv,
  cmake,
  extra-cmake-modules,
  plasma-framework,
  kwindowsystem,
  lib,
  fetchFromGitHub,
  writeText,
  zip,
  which
}:
let
  version = "76";
  CMakeLists = writeText "CMakeLists.txt" (builtins.readFile ./CMakeLists.txt);
in
stdenv.mkDerivation {
  pname = "plasma-applet-eventcalendar";
  inherit version;

  src = fetchFromGitHub {
    owner = "akshaymankar";
    repo = "plasma-applet-eventcalendar";
    rev = "83af9cfcba8bdff5313de5f7a6db27ca1218dff9";
    sha256 = "sha256-pUYmbcmgXe0jcx6RtuatTyrxawetYpe8AtoWL5gYGlA=";
  };

  buildPhase = ''
  ls -la
  patchShebangs build
  bash -x build
  plasmapkg2 --type Plasma/Applet --install *.plasmoid --packageroot=$out/share/plasma/plasmoids/
  '';


 #  patchPhase = ''
 #    rm -f build install uninstall update
 #    cp ${CMakeLists} CMakeLists.txt
 # '';

  # nativeBuildInputs = [
  #   cmake
  #   extra-cmake-modules
  # ];

  nativeBuildInputs = [ plasma-framework ];

  buildInputs = [
    plasma-framework
    kwindowsystem
    zip
  ];

  dontWrapQtApps = true;

  meta = with lib; {
    description = "KDE Plasma 5 widget for showing event calendar";
    homepage = "https://github.com/Zren/plasma-applet-eventcalendar";
    license = licenses.gpl2Plus;
    platforms = platforms.linux;
    maintainers = with maintainers; [ benley zraexy ];
  };
}
