self: super: {
  plasma-applet-eventcalendar = super.libsForQt5.callPackage ./plasma-applet-eventcalendar {};
  wire-desktop-internal = super.callPackage ./wire-desktop-internal {};
  lua-ats = super.callPackage ./lua-ats {
    inherit (super.lua53Packages) wrapLua;
    lua = super.lua5_3;
  };
  grafana-dashboards = super.callPackage ./grafana-dashboards {};
  vault-scripts = super.callPackage ./vault-scripts {};
  bao-vault = super.callPackage ./bao-vault {};
}
