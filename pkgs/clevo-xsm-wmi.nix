{ stdenv, lib, fetchurl, kernel }:
stdenv.mkDerivation rec {
  name = "clevo-xsm-wmi-${version}-${kernel.version}";
  version = "1.1.1"; # Patched

  src = fetchurl {
    url = "https://git.coop/akshay/clevo-xsm-wmi/-/archive/6f97db8465e05802d41c530d1695c3cc19741d64/clevo-xsm-wmi.tar.gz";
    sha256 = "1r5b8rkjsds6hkv3z8iw0xk2mlyidc999nik6202xzm6gxhn3qs8";
  };

  sourceRoot = "clevo-xsm-wmi-6f97db8465e05802d41c530d1695c3cc19741d64/module";
  hardeningDisable = [ "pic" "format" ];
  nativeBuildInputs = kernel.moduleBuildDependencies;

  makeFlags = [
    "KDIR=${kernel.dev}/lib/modules/${kernel.modDirVersion}/build"
    "INSTALL_MOD_PATH=$(out)"
  ];
}
