{openbao, runCommand}:
runCommand "bao-vault" {} ''
  mkdir -p $out/bin
  ln -s ${openbao}/bin/bao $out/bin/vault
''
