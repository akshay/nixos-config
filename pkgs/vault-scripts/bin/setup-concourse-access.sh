#!/usr/bin/env bash

set -euo pipefail

ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" &> /dev/null && pwd )"
export VAULT_CACERT=/var/keys/vault-ca

vault secrets enable -version=2 -path=concourse kv
vault policy write concourse "$ROOT_DIR/config/concourse-policy.hcl"
token=$(vault token create --policy concourse --period 1h -format json \
            | jq -r .auth.client_token)

mkdir -p /var/generated-keys/
echo "$token" > /var/generated-keys/concourse-vault-client-token
echo "New client token for concourse saved. 'concourse-web' needs to be restarted."
