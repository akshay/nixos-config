{
  srcOnly,
  writeShellScriptBin,
  symlinkJoin,

  openbao,
  postgresql,
  jq
}:
let
  vault-scripts = srcOnly {
    name = "vault-scripts";
    src = ./.;
  };

  mkBin = name: writeShellScriptBin name ''
    set -euo pipefail
    export PATH="${vault-scripts}/bin:${openbao}/bin:${postgresql}/bin:${jq}/bin:$PATH"
    ${name}.sh
  '';

  reset-vault = mkBin "reset-vault";
  setup-concourse-access = mkBin "setup-concourse-access";
in
symlinkJoin {
  name = "vault-scripts-bin";
  paths = [
    reset-vault
    setup-concourse-access
  ];
}
