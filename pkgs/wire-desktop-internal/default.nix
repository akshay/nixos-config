{wire-desktop, fetchurl}:
# NOTE: This is the internal development-build of wire. It is not meant for
# public consumption and it comes with NO warranty whatsoever
wire-desktop.overrideAttrs (old: old // rec {
  version = "3.39.135~internal-135";
  src = fetchurl {
    url = "https://wire-app.wire.com/linux-internal/debian/pool/main/WireInternal-3.39.135-internal_amd64.deb";
    sha256 = "1frxm0qm3bybhyjz241yn51rpmfg8p0l9k3wni5bjjsm2fhp5q9j";
  };
  postFixup = ''
    makeWrapper $out/opt/WireInternal/wire-desktop-internal $out/bin/wire-desktop-internal  "''${gappsWrapperArgs[@]}"
    sed -i "s|/opt/WireInternal/wire-desktop-internal|$out/bin/wire-desktop-internal|g" "$out/share/applications/wire-desktop-internal.desktop"
  '';
})
