{ stdenv
, fetchFromGitHub
, lua
, gnused
, wrapLua
}:
stdenv.mkDerivation rec {
  pname = "ats";
  version = "0.2.0";

  src = fetchFromGitHub {
    owner = "tuxd3v";
    repo = pname;
    rev = "v${version}";
    sha256 = "sha256-HjDJ6bRRcWiUC0QamCF+ZYjoGiL098Txj9MApFd2CiM=";
  };

  buildInputs = [ wrapLua ];

  propagatedBuildInputs = [ lua ];

  makeFlags = [
    "DEPS=lua"
    "IDIR=${lua}/include"
    "LDIR=${lua}/lib"
    "BINDIR=$(out)/bin"
    "CONFDIR=$(out)/etc"
    "SYSTEMDIR=$(out)/lib/systemd/system"
  ];

  preBuild = ''
    ${gnused}/bin/sed -i "s|#include <lua5.3/|#include <|" src/ats.c
    ${gnused}/bin/sed -i "s|#include <lua5.3/|#include <|" src/debug.c
    ${gnused}/bin/sed -i "s|/etc/ats.conf|$out/etc/ats.conf|" src/ats
    ${gnused}/bin/sed -i "s|/sys/class/hwmon/hwmon0/pwm1|/sys/devices/platform/pwm-fan/hwmon/hwmon3/pwm1|" etc/ats.conf
    ${gnused}/bin/sed -i "s|PROFILE_NAME\s\+= \"profile0\"|PROFILE_NAME = \"profile2\"|" etc/ats.conf
    ${gnused}/bin/sed -i "s|PROFILE_NR\s\+= 0|PROFILE_NR = 2|" etc/ats.conf

    ${gnused}/bin/sed -i "s|--owner root --group root ||" Makefile
  '';

  installPhase = ''
    runHook preInstall
    mkdir -p $out/bin $out/etc
    install -Dm755 -t $out/bin src/ats
    install -Dm644 -t $out/etc etc/ats.conf
    install -Dm644 -t $out/lib/lua/5.3 ats.so.0.9
    ln -s $out/lib/lua/5.3/ats.so.0.9 $out/lib/lua/5.3/ats.so
    runHook postInstall
  '';

  postFixup = ''
    wrapLuaPrograms
  '';
}

