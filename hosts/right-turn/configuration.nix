{ config, pkgs, lib, ... }:
{
  imports = [
    ./hardware-configuration.nix
    ./wifi.nix
  ];

  boot.loader.grub.enable = false;
  boot.loader.generic-extlinux-compatible.enable = true;
  boot.kernelPackages = pkgs.linuxPackages_rpi3;

  nixpkgs.hostPlatform = lib.mkDefault "aarch64-linux";

  networking.hostName = "right-turn";

  users.users.root.openssh.authorizedKeys.keys = [
    (builtins.readFile ../../public-keys/axeman)
  ];

  users.users.axeman = {
    isNormalUser = true;
    extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keys = [
      (builtins.readFile ../../public-keys/axeman)
    ];
  };

  services = {
    openssh.enable = true;
  };

  environment.systemPackages = [
    pkgs.gpio-utils
    pkgs.libgpiod
    pkgs.htop
  ];

  programs.git.enable = true;
  programs.neovim = {
    enable = true;
    vimAlias = true;
    viAlias = true;
    configure = {
      packages.myVimPackages = with pkgs.vimPlugins; {
        start = [vim-nix vim-surround rainbow];
      };
    };
  };

  system.stateVersion = "24.11";
}
