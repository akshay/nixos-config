{...}:
{

  age.secrets = {
    wifi-secrets = {
      file = ../../age/home-wifi-psk.age;
      mode = "0400";
      owner = "root";
      group = "root";
    };
  };

  networking.networkmanager.enable = false;
  networking.wireless = {
    enable = true;
    secretsFile = "/run/agenix/wifi-secrets";
    networks."You would not download a WiFi" = {
      pskRaw = "ext:psk_home";
    };
  };
}
