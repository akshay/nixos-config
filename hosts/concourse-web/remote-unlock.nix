{...}:
{
  # cryptsetup-askpass
  # zpool import -a
  # zfs load-key deadpool/encrypted
  boot.initrd = {
    availableKernelModules = [ "r8169" ];
    supportedFilesystems = ["zfs"];
    luks.devices = {
      root.device = "/dev/disk/by-id/scsi-0QEMU_QEMU_HARDDISK_57802441-part2";
    };
    network = {
      enable = true;
      udhcpc.enable = true;
      flushBeforeStage2 = true;
      ssh = {
        enable = true;
        port = 22;
        authorizedKeys = [
          (builtins.readFile ../../public-keys/axeman)
        ];
        hostKeys = [ "/etc/secrets/initrd/ssh_host_ed25519_key" ];
      };
    };
  };
}
