{ pkgs, config, modulesPath, ... }:
let
  vault-migrations = pkgs.vault-migrations;
  vault-scripts = pkgs.vault-scripts;
  tsa-authorized-keys = pkgs.concatTextFile {
    name = "concourse-tsa-authorized-keys";
    files = [
      ../../public-keys/concourse-worker-1-tsa-key
    ];
  };
  # servers = pkgs.lib.trivial.importJSON ../data/generated/servers.json;
in {
  imports = [
    ./hardware-configuration.nix
    ./remote-unlock.nix
  ];

  system.stateVersion = "24.11";

  boot.cleanTmpDir = true;
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.supportedFilesystems = [ "zfs" ];
  boot.zfs.forceImportRoot = false;

  networking.hostId = "02581e50";
  networking.hostName = "concourse-web";
  networking.firewall.allowPing = true;
  networking.firewall.allowedTCPPorts = [ 22 80 443 2222 ];

  networking.interfaces.enp1s0.ipv6.addresses = [{
    address = "2a01:4f8:1c1b:efef::1";
    prefixLength = 64;
  }];
  networking.defaultGateway6 = { address = "fe80::1"; interface = "enp1s0"; };

  services.openssh.enable = true;

  users.users.root.openssh.authorizedKeys.keys = [
    (builtins.readFile ../../public-keys/axeman)
  ];

  environment.systemPackages = [ vault-scripts pkgs.openbao ];

  services.postgresql = {
    enable = true;
    package = pkgs.postgresql_16;
    ensureDatabases = [ "concourse" "vault" ];
    dataDir = "/data/postgresql";
    ensureUsers = [{
      name = "concourse";
      ensureDBOwnership = true;
    } {
      name = "vault";
      ensureDBOwnership = true;
    }];
  };

  # Hashicorp Vault

  services.vault = {
    enable = true;
    package = pkgs.bao-vault;
    storageBackend = "postgresql";
    storageConfig = ''
      connection_url = "postgres:///vault?sslmode=disable&host=/run/postgresql"
    '';
    # tlsCertFile = "/var/keys/vault-server-cert";
    # tlsKeyFile = "/var/keys/vault-server-cert-key";
  };

  age.secrets = {
    concourse-tsa-host-key = {
      file = ../../age/concourse-tsa-host-key.age;
      mode = "0400";
      owner = "concourse";
      group = "concourse";
    };
    concourse-session-signing-key = {
      file = ../../age/concourse-session-signing-key.age;
      mode = "0400";
      owner = "concourse";
      group = "concourse";
    };
  };

  # users.users.concourse = {
  #   description = "User to run concourse";
  #   group = "concourse";
  #   extraGroups = ["keys"];
  #   isSystemUser = true;
  # };

  # users.groups.concourse = {};

  # users.users.vault.extraGroups = [ "keys" ];

  services.concourse-web = {
    enable = true;
    postgres.socket =  "/run/postgresql";
    postgres.database = "concourse";
    tsa.authorized-keys = "${tsa-authorized-keys}";
    tsa.host-key = "/run/agenix/concourse-tsa-host-key";
    session-signing-key = "/run/agenix/concourse-session-signing-key";
    # tls-bind-port = 443;
    external-url = "https://concourse.gdn";
    # enable-lets-encrypt = true;
    enable-redact-secrets = true;
    enable-across-step = true;
    enable-pipeline-instances = true;
    enable-resource-causality = true;

    extraEnvVars = {
      CONCOURSE_GITLAB_HOST = "https://git.coop";
      CONCOURSE_MAIN_TEAM_GITLAB_USER = "akshay";
      CONCOURSE_VAULT_URL = "http://localhost:8200";
      # CONCOURSE_VAULT_CA_CERT = "/var/keys/vault-ca";
    };
  };

  # systemd.services.concourse-web = {
  #   enable = true;
  #   wantedBy = [ "multi-user.target" ];
  #   requires = [ "postgresql.service" ];
  #   description = "Concourse Web";
  #   confinement = {
  #     enable = true;
  #     packages = [ pkgs.concourse pkgs.cacert pkgs.openssl ];
  #   };
  #   script = ''
  #     set -euo pipefail

  #     cat="${pkgs.coreutils}/bin/cat"

  #     CONCOURSE_ENCRYPTION_KEY=$($cat /var/keys/concourse-encryption-key)

  #     CONCOURSE_GITLAB_CLIENT_ID=$($cat /var/keys/concourse-gitlab-client-id)
  #     CONCOURSE_GITLAB_CLIENT_SECRET=$($cat /var/keys/concourse-gitlab-client-secret)

  #     CONCOURSE_VAULT_CLIENT_TOKEN=$($cat /var/generated-keys/concourse-vault-client-token)

  #     export CONCOURSE_ENCRYPTION_KEY CONCOURSE_GITLAB_CLIENT_ID CONCOURSE_GITLAB_CLIENT_SECRET CONCOURSE_VAULT_CLIENT_TOKEN

  #     ${pkgs.concourse}/bin/concourse web
  #   '';
  #   serviceConfig = {
  #     User = "concourse";
  #     BindReadOnlyPaths = [
  #       "/var/keys/concourse-session-signing-key"
  #       "/var/keys/concourse-tsa-host-key"
  #       "/var/keys/concourse-encryption-key"
  #       "/var/keys/concourse-tsa-authorized-keys"
  #       "/var/keys/concourse-gitlab-client-id"
  #       "/var/keys/concourse-gitlab-client-secret"
  #       "/var/keys/vault-ca"
  #       "/var/generated-keys/concourse-vault-client-token"
  #       "/etc/resolv.conf"
  #       "${config.environment.etc."ssl/certs/ca-certificates.crt".source}:/etc/ssl/certs/ca-certificates.crt"
  #     ];
  #     BindPaths = "/run/postgresql";
  #     CapabilityBoundingSet = "cap_net_bind_service";
  #     AmbientCapabilities = "cap_net_bind_service";
  #     NoNewPrivileges = true;
  #     Type = "simple";

  #     # Implied true by confinement.enable, but it must be false for ambient
  #     # capabilties to work.
  #     PrivateUsers = false;
  #   };
  #   environment = {
  #     CONCOURSE_GITLAB_HOST = "https://git.coop";
  #     CONCOURSE_MAIN_TEAM_GITLAB_USER = "akshay";
  #     CONCOURSE_POSTGRES_DATABASE = "concourse";
  #     CONCOURSE_POSTGRES_SOCKET = "/run/postgresql";
  #     CONCOURSE_SESSION_SIGNING_KEY = "/var/keys/concourse-session-signing-key";
  #     CONCOURSE_TSA_HOST_KEY = "/var/keys/concourse-tsa-host-key";
  #     CONCOURSE_TSA_AUTHORIZED_KEYS = "/var/keys/concourse-tsa-authorized-keys";
  #     CONCOURSE_TLS_BIND_PORT = "443";
  #     CONCOURSE_EXTERNAL_URL = "https://concourse.gdn";
  #     CONCOURSE_ENABLE_LETS_ENCRYPT = "true";
  #     CONCOURSE_VAULT_URL = "https://localhost:8200";
  #     CONCOURSE_VAULT_CA_CERT = "/var/keys/vault-ca";
  #     CONCOURSE_ENABLE_REDACT_SECRETS = "true";
  #     CONCOURSE_ENABLE_ACROSS_STEP = "true";
  #     CONCOURSE_ENABLE_PIPELINE_INSTANCES = "true";
  #     CONCOURSE_ENABLE_RESOURCE_CAUSALITY = "true";
  #   };
  # };
}
