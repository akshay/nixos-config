{ config, pkgs, ... }:

{
  home.username = "axeman";
  home.homeDirectory = "/home/axeman";
  home.stateVersion = "22.05";

  nixpkgs.config = {
    allowUnfree = true;
    firefox.enablePlasmaBrowserIntegration = true;
    permittedInsecurePackages = [
      "olm-3.2.16"
    ];
  };

  home.packages = with pkgs; [
    rage
    gnupg
    wget
    htop
    keepassxc
    nerd-fonts.fira-code
    nix-index
    open-sans
    niv
    xsel # Required for tmux plugin yank
    wire-desktop
    zoom-us
    ncspot
    ripgrep
    python3
    vlc
    dig
    cachix
    slack
    discord
    okular
    glade
    tor-browser-bundle-bin
    libreoffice
    (haskellPackages.ghcWithPackages(p: [p.network p.http-conduit p.feed]))
    haskell-language-server
    quassel
    nheko
    docker-compose
    dhall
    nodePackages.js-beautify
    gnome-disk-utility
    signal-desktop
    tdesktop
    inotify-tools
    gnucash
    unzip
    sqlite
    jq
    libsForQt5.gwenview
    libsForQt5.kitinerary
    libsForQt5.ktrip
    drone-cli
    plasma-applet-eventcalendar
    qpwgraph
    dasel
    pciutils
    usbutils
    openssl
    nix-output-monitor

    radare2

    # Rust
    rust-analyzer
    cargo
    rustc
    clippy
    rustfmt

    # Go
    go
    gopls
    gotools
    reftools

    # Other stuff
    element-desktop
  ];

  fonts.fontconfig.enable = true;

  programs = {
    direnv = {
      enable = true;
      enableZshIntegration = true;
      nix-direnv.enable = true;
    };
    home-manager.enable = true;
    password-store.enable = true;
    firefox.enable = true;
    browserpass = {
      enable = true;
      browsers = ["firefox"];
    };

    emacs = {
      enable = true;
      package = pkgs.emacs29-pgtk;
    };

    alacritty = {
      enable = true;
      settings = {
        cursor.style = "Beam";
        general.live_config_reload = true;
        font = {
          normal.family = "Fira Code Nerd Font";
          bold.family = "Fira Code Nerd Font";
          bold.style = "Bold";
          italic.family = "Fira Code Nerd Font";
          italic.style = "Italic";
        };
        window = {
          startup_mode = "Maximized";
          decorations = "none";
        };
        colors = {
          primary = {
            background = "0x282a36";
            foreground = "0xf8f8f2";
          };
          normal = {
            black =   "0x000000";
            red =     "0xff5555";
            green =   "0x50fa7b";
            yellow =  "0xf1fa8c";
            blue =    "0xbd93f9";
            magenta = "0xff79c6";
            cyan =    "0x8be9fd";
            white =   "0xbbbbbb";
          };
          # Bright colors
          bright = {
            black =   "0x555555";
            red =     "0xff5555";
            green =   "0x50fa7b";
            yellow =  "0xf1fa8c";
            blue =    "0xcaa9fa";
            magenta = "0xff79c6";
            cyan =    "0x8be9fd";
            white =   "0xffffff";
          };
        };
      };
    };

    neovim = {
      enable = true;
      plugins = with pkgs.vimPlugins; [
        yankring
        vim-nix
      ];
      viAlias = true;
      vimAlias = true;
      vimdiffAlias = true;
    };

    git = import ../../../configs/git.nix;

    zsh = {
      enable = true;
      localVariables = { POWERLEVEL9K_CONFIG_FILE = "/home/axeman/.p10k.zsh"; };
      initExtra = "source /home/axeman/.p10k.zsh";
      prezto = {
        enable = true;
        prompt.theme = "powerlevel10k";
        pmodules = [
          "environment"
          "terminal"
          "editor"
          "history"
          "directory"
          "spectrum"
          "utility"
          "completion"
          "prompt"
          "syntax-highlighting"
          "history-substring-search"
        ];
        autosuggestions.color = "fg=blue";
        syntaxHighlighting.highlighters = [ "main" "brackets" "pattern" "line" "root" ];
      };
    };

    tmux = {
      enable = true;
      secureSocket = false;
      # After 21.03: Replace with: prefix = "C-space";
      shortcut = "space";
      escapeTime = 0;
      baseIndex = 1;
      keyMode = "vi";
      terminal = "screen-256color";
      plugins = with pkgs.tmuxPlugins; [
        continuum
        cpu
        fingers
        pain-control
        prefix-highlight
        sensible
        yank
      ];
      extraConfig = builtins.readFile ./tmux.conf;
    };
  };

  services = {
    gpg-agent = {
      enable = true;
      pinentryPackage = null ;
      defaultCacheTtl = 1800; # 30 minutes
      extraConfig = ''
        pinentry-program ${pkgs.kwalletcli}/bin/pinentry-kwallet
      '';
    };
    syncthing = {
      enable = true;
    };
    nextcloud-client = {
      enable = true;
    };
    kdeconnect = {
      enable = true;
      indicator = true;
    };
    spotifyd = {
      enable = true;
      package = (pkgs.spotifyd.override { withKeyring = true; withMpris = true; });
      settings = {
        global = {
          device_name = "tuxnix";
          zeroconf_port = 5907;
        };
      };
    };
  };
}
