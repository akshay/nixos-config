# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ../../modules/pipewire-bluetooth
      ../../modules/pipewire-combined-sink
      ../../modules/pipewire-noise-cancelling-source
      ./pub.solar-wireguard.nix
    ];

  # Enable Flakes
  nix = {
    package = pkgs.nixVersions.latest;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
    settings = {
      trusted-users = [ "root" "axeman" ];
    };
   };

  # Allow unfree for steam
  nixpkgs.config.allowUnfree = true;

  # Kernel
  boot.kernelPackages = pkgs.linuxPackages_6_1.extend (self: super: {
    tuxedo-keyboard = super.tuxedo-keyboard.overrideAttrs (o: rec {
      version = "3.1.1";
      src = pkgs.fetchFromGitHub {
        owner = "tuxedocomputers";
        repo = "tuxedo-keyboard";
        rev = "v${version}";
        sha256 = "sha256-+59/5vfwx9fys7Q63SahVPS/ckvwkr4w6T37UqAnwZ4=";
      };
    });
  });

  # Keboard
  hardware.tuxedo-keyboard.enable = true;

  # Tuxedo Control Center
  # hardware.tuxedo-control-center.enable = true;

  # Bluetooth
  hardware.bluetooth.enable = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # Allow building for aarch64-linux
  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];

  # Allow NTFS
  boot.supportedFilesystems = [ "ntfs" ];

  networking.hostName = "tuxnix"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable = true;

  networking.wireguard.enable = true;

  networking.extraHosts = ''
    192.168.122.82 social-coop-test.liberated.cloud
  '';

  # Set your time zone.
  # time.timeZone = "Europe/Amsterdam";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;

  # # Remove these because they clash with network manager
  # networking.interfaces.eno1.useDHCP = true;
  # networking.interfaces.wlp1s0.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  # i18n.defaultLocale = "en_US.UTF-8";
  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  # };

  # Enable the Plasma 5 Desktop Environment.
  services.xserver.enable = true;
  services.xserver.videoDrivers = [ "amdgpu" ];
  services.displayManager.sddm.enable = true;
  services.displayManager.sddm.wayland.enable = true;
  services.desktopManager.plasma6.enable = true;
  services.xserver.desktopManager.gnome.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  services.libinput.enable = true;

  # Configure keymap in X11
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound.
  hardware.pulseaudio.enable = false;

  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
  };

  services.jellyfin.enable = true;

  # Enable ADB for Android hacking
  programs.adb.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  # users.users.jane = {
  #   isNormalUser = true;
  #   extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
  # };
  users.users.axeman = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" "audio" "docker" "adbusers" "jackaudio" ];
    shell = pkgs.zsh;
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  # environment.systemPackages = with pkgs; [
  #   wget vim
  #   firefox
  # ];
  environment.systemPackages = [
    pkgs.libsForQt5.korganizer
    pkgs.libsForQt5.akonadi
    pkgs.libsForQt5.merkuro
    pkgs.libsForQt5.kontact
    pkgs.libsForQt5.kdepim-runtime
    pkgs.libsForQt5.kdepim-addons
    pkgs.libsForQt5.kaccounts-integration
    pkgs.libsForQt5.kaccounts-providers
    pkgs.libsForQt5.akonadi-calendar-tools
    pkgs.libsForQt5.kmail
    pkgs.virt-manager
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };
  # programs.nm-applet.enable = true;
  programs.zsh.enable = true;
  programs.steam.enable = true;
  programs.git.enable = true;
  programs.neovim = {
    enable = true;
    vimAlias = true;
    viAlias = true;
    configure = {
      packages.myVimPackages = with pkgs.vimPlugins; {
        start = [vim-nix vim-surround rainbow];
      };
    };
  };

  programs.ssh.enableAskPassword = false;

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  services.localtimed.enable = false;
  services.avahi.enable = true;

  # Docker
  virtualisation.docker.enable = true;

  # libvirtd
  virtualisation.libvirtd.enable = true;
  virtualisation.spiceUSBRedirection.enable = true;
  programs.dconf.enable = true;

  # Waydroid
  virtualisation.waydroid.enable = true;

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [
    #Jellyfin
    8096

    # Miracast
    7236
    7250

    # Spotifyd
    5907
  ];

  networking.firewall.allowedUDPPorts = [
    #Jellyfin
    1900

    # Miracast
    7236
    5353
  ];

  # kdeconnect
  networking.firewall.allowedTCPPortRanges = [{
    from = 1714;
    to = 1764;
  }];
  networking.firewall.allowedUDPPortRanges = [{
    from = 1714;
    to = 1764;
  }];

  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  systemd.coredump.extraConfig = ''
    ProcessSizeMax=8G
    ExternalSizeMax=8G
    JournalSizeMax=8G
    '';

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.09"; # Did you read the comment?

}

