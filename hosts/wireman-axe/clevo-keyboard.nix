{ config, pkgs, ... }:

let
  clevo-xsm-wmi = import ../../pkgs/clevo-xsm-wmi.nix {
    stdenv = pkgs.stdenv;
    lib = pkgs.lib;
    fetchurl = pkgs.fetchurl;
    kernel = pkgs.linux; # Ensure this matches boot.kernelPackages
  };
in {
  boot.extraModulePackages = [ clevo-xsm-wmi ];
  boot.kernelModules = [ "clevo-xsm-wmi" ];
}
