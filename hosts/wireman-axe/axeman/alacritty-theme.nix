let
  dracula = {
    primary = {
      background = "0x282a36";
      foreground = "0xf8f8f2";
    };
    normal = {
      black =   "0x000000";
      red =     "0xff5555";
      green =   "0x50fa7b";
      yellow =  "0xf1fa8c";
      blue =    "0xbd93f9";
      magenta = "0xff79c6";
      cyan =    "0x8be9fd";
      white =   "0xbbbbbb";
    };
    bright = {
      black =   "0x555555";
      red =     "0xff5555";
      green =   "0x50fa7b";
      yellow =  "0xf1fa8c";
      blue =    "0xcaa9fa";
      magenta = "0xff79c6";
      cyan =    "0x8be9fd";
      white =   "0xffffff";
    };
  };
  pencil-light = {
    primary = {
      background = "0xf1f1f1";
      foreground = "0x424242";
    };
    normal= {
      black = "0x212121";
      red = "0xc30771";
      green = "0x10a778";
      yellow = "0xa89c14";
      blue = "0x008ec4";
      magenta = "0x523c79";
      cyan = "0x20a5ba";
      white = "0xe0e0e0";
    };
    bright = {
      black = "0x212121";
      red = "0xfb007a";
      green = "0x5fd7af";
      yellow = "0xf3e430";
      blue = "0x20bbfc";
      magenta = "0x6855de";
      cyan = "0x4fb8cc";
      white = "0x222222"; # Changed
    };
  };
in dracula
