{ config, pkgs, unstable, home-manager-unstable,  ... }:

{
  # https://github.com/nix-community/home-manager/issues/3696#issuecomment-1901106102
  imports = [
    "${home-manager-unstable}/modules/programs/alacritty.nix"
  ];
  disabledModules = ["programs/alacritty.nix"];

  home.username = "axeman";
  home.homeDirectory = "/home/axeman";
  home.stateVersion = "21.03";

  home.packages = with pkgs; [
    gnupg
    htop
    keepassxc
    (nerdfonts.override { fonts = ["FiraCode"]; })
    emacs-all-the-icons-fonts
    niv
    xsel # Required for tmux plugin yank
    # wire-desktop
    docker
    docker-compose
    slack
    tmate
    dnsutils
    ripgrep
    python3
    cachix
    unstable.dhall
    unstable.dhall-json
    symbola
    libreoffice
    unstable.wire-desktop-internal
    libsForQt5.ark
    ormolu
    terraform
    unstable.jq
    unstable.shellcheck
    unstable.nodePackages.bash-language-server
    k9s
    nodejs
    unstable.zoom-us
    diff-so-fancy
    nixfmt

    # haskell stuff
    unstable.ghc
    unstable.haskell-language-server
    unstable.cabal-install
    zlib
    openssl
    pkg-config
  ];

  fonts.fontconfig.enable = true;

  programs = {
    direnv = {
      enable = true;
      enableZshIntegration = true;
      nix-direnv.enable = true;
    };
    home-manager.enable = true;
    emacs = {
      enable = true;
      package = pkgs.emacs29;
    };
    password-store.enable = true;
    firefox.enable = true;

    alacritty = {
      enable = true;
      package = unstable.alacritty;
      settings = {
        cursor.style = "Beam";
        general.live_config_reload = true;
        font = {
          normal.family = "Fira Code Nerd Font";
          bold.family = "Fira Code Nerd Font";
          bold.style = "Bold";
          italic.family = "Fira Code Nerd Font";
          italic.style = "Italic";
          size = 14;
        };
        window = {
          startup_mode = "Maximized";
          decorations = "none";
        };
        colors = import ./alacritty-theme.nix;
      };
    };

    neovim = {
      enable = true;
      plugins = with pkgs.vimPlugins; [
        yankring
        vim-nix
        vim-markdown
        vim-markdown-toc
        coc-nvim
        coc-go
        nerdtree
        vim-surround
      ];
      viAlias = true;
      vimAlias = true;
      vimdiffAlias = true;
    };

    git = import ../../../configs/git.nix;

    zsh = {
      enable = true;
      localVariables = { POWERLEVEL9K_CONFIG_FILE = "/home/axeman/.p10k.zsh"; };
      initExtra = "source /home/axeman/.p10k.zsh";
      prezto = {
        enable = true;
        prompt.theme = "powerlevel10k";
        pmodules = [
          "environment"
          "terminal"
          "editor"
          "history"
          "directory"
          "spectrum"
          "utility"
          "completion"
          "prompt"
          "syntax-highlighting"
          "history-substring-search"
        ];
        autosuggestions.color = "fg=blue";
        syntaxHighlighting.highlighters = [ "main" "brackets" "pattern" "line" "root" ];
      };
    };

    tmux = {
      enable = true;
      secureSocket = false;
      # After 21.03: Replace with: prefix = "C-space";
      shortcut = "space";
      escapeTime = 0;
      baseIndex = 1;
      keyMode = "vi";
      terminal = "screen-256color";
      plugins = with pkgs.tmuxPlugins; [
        continuum
        cpu
        fingers
        pain-control
        prefix-highlight
        sensible
        yank
      ];
      extraConfig = builtins.readFile ./tmux.conf;
    };
  };

  services = {
    gpg-agent = {
      enable = true;
      defaultCacheTtl = 600; # 10 minutes
      pinentryPackage = null;
      extraConfig = ''
        pinentry-program ${pkgs.kwalletcli}/bin/pinentry-kwallet
      '';
    };
    kdeconnect = {
      enable = true;
      indicator = true;
    };
  };
}
