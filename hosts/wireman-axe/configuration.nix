# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, personalLaptop, ... }:

let
  commonImports = [
      ../../modules/pipewire-bluetooth
      ../../modules/pipewire-combined-sink
      ../../modules/openfortivpn
      ];
  specificImports = if personalLaptop
      then [
        ./hardware-configuration-personal.nix
        ./amdgpu.nix
      ]
      else [
        ./hardware-configuration.nix
        ./nvidia.nix
        ./clevo-keyboard.nix
      ];
in
{
  imports =
    commonImports ++ specificImports;

  nix = {
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
    settings = {
      trusted-users = [ "root" "axeman" ];
    };
  };

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # Allow building for aarch64-linux
  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];

  # Allow NTFS
  boot.supportedFilesystems = [ "ntfs" ];

  hardware.bluetooth.enable = true;

  # Allow unfree for nvidia
  nixpkgs.config.allowUnfree = true;
  nixpkgs.config.packageOverrides = pkgs: {
    steam = pkgs.steam.override {
      extraPkgs = pkgs: with pkgs; [
        faudio
      ];
    };
  };

  programs.steam.enable = true;

  # networking.hostName = "nixos"; # Define your hostname.
  networking.hostName = "wireman-axe";
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable = true;

  # Set your time zone.
  # time.timeZone = "Europe/Amsterdam";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  # networking.interfaces.enp116s0.useDHCP = true;
  # networking.interfaces.wlp122s0.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  networking.extraHosts = ''
    127.0.0.1 local.zinfra.io
    ::1 local.zinfra.io
  '';

  # Desktop Environment.
  services.xserver.enable = true;
  services.displayManager.sddm.enable = true;
  services.displayManager.sddm.wayland.enable = true;
  services.desktopManager.plasma6.enable = true;

  # Enable CUPS to print documents.
  services.printing.enable = true;
  services.printing.browsedConf = ''
    BrowseDNSSDSubTypes _cups,_print
    BrowseLocalProtocols all
    BrowseRemoteProtocols all
    CreateIPPPrinterQueues All

    BrowseProtocols all
  '';
  services.avahi = {
    enable = true;
    nssmdns4 = true;
  };

  # Enable sound.
  hardware.pulseaudio.enable = false;
  environment.etc = {
	  "wireplumber/bluetooth.lua.d/51-bluez-config.lua".text = ''
		  bluez_monitor.properties = {
		  	["bluez5.enable-sbc-xq"] = true,
		  	["bluez5.enable-msbc"] = true,
		  	["bluez5.enable-hw-volume"] = true,
		  	["bluez5.headset-roles"] = "[ hsp_hs hsp_ag hfp_hf hfp_ag ]"
		  }
	  '';
  };
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  services.libinput.enable = true;

  # Enable android adb
  services.udev.packages = [ pkgs.android-udev-rules ];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  # users.users.jane = {
  #   isNormalUser = true;
  #   extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
  # };
  users.users.axeman = {
    isNormalUser = true;
    extraGroups = [ "docker" "wheel" ];
    shell = pkgs.zsh;
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  # environment.systemPackages = with pkgs; [
  #   wget vim
  #   firefox
  # ];
  environment.systemPackages = [
    pkgs.firefox
    pkgs.gwenview
    pkgs.git
  ];

  virtualisation.docker.enable = true;
  virtualisation.containerd.enable = true;
  virtualisation.waydroid.enable = true;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };
  programs.zsh.enable = true;

  programs.neovim = {
    enable = true;
    defaultEditor = true;
    vimAlias = true;
    viAlias = true;
    configure = {
      packages.myVimPackage = with pkgs.vimPlugins; {
        start = [vim-nix];
      };
    };
  };

  programs.ssh.enableAskPassword = false;

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 22 ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # kdeconnect
  networking.firewall.allowedTCPPortRanges = [ {
    from = 1714;
    to = 1764;
  }];
  networking.firewall.allowedUDPPortRanges = [ {
    from = 1714;
    to = 1764;
  }];

  networking.firewall.extraCommands = "iptables -A INPUT -p tcp --destination-port 8443 -s 172.16.0.0/12 -j ACCEPT";


  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.09"; # Did you read the comment?
}
