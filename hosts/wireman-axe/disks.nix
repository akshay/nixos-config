{...}:
{
  fileSystems."/" =
    { device = "/dev/disk/by-uuid/d8ffd25c-f2b8-4798-a159-9a185186c4b5";
      fsType = "ext4";
    };

  boot.initrd.luks.devices."crypted".device = "/dev/disk/by-uuid/77e7a4d4-6099-4b25-bc25-a0e8a5cf3b40";

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/6AA1-4EEB";
      fsType = "vfat";
    };

  swapDevices =
    [ { device = "/dev/disk/by-uuid/c6dddba6-072c-4235-9e39-52bf4d73513b"; }
    ];
}
