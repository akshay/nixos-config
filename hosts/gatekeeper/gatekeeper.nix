{ pkgs, ...}:
{
  system.stateVersion = "22.11";

  users.users.rathole = {
    description = "User to run rathole";
    group = "rathole";
    isSystemUser = true;
    home = "/var/lib/rathole";
    createHome = true;
  };

  users.groups.rathole = {};

  age.secrets = {
    rathole-noise-private-key = {
      file = ../../age/rathole-gatekeeper-noise-private-key.age;
      mode = "0400";
      owner = "rathole";
      group = "rathole";
    };

    rathole-moonrock-grafana-token = {
      file = ../../age/rathole-moonrock-grafana-token.age;
      mode = "0400";
      owner = "rathole";
      group = "rathole";
    };

    rathole-moonrock-nginx-plain-token = {
      file = ../../age/rathole-moonrock-nginx-plain-token.age;
      mode = "0400";
      owner = "rathole";
      group = "rathole";
    };

    rathole-moonrock-nginx-tls-token = {
      file = ../../age/rathole-moonrock-nginx-tls-token.age;
      mode = "0400";
      owner = "rathole";
      group = "rathole";
    };

    openvpn-moonrock-boot-key = {
      file = ../../age/openvpn-moonrock-boot-key.age;
      mode = "0400";
      owner  = "root";
      group = "root";
    };
  };

  services.openvpn.servers = {
    bootVPN  = {
      config = ''
        dev tun
        ifconfig 10.8.0.1 10.8.0.2
        secret /run/agenix/openvpn-moonrock-boot-key
      '';
    };
  };

  systemd.services.rathole =
    let
      conf = pkgs.writeText "rathole-server.toml" (builtins.readFile ./rathole-config/server.toml);
    in {
      enable = true;
      description = "Rathole - Reverse proxy for NAT traversal";
      wantedBy = [ "basic.target" ];
      confinement = {
        enable = true;
        packages = [ pkgs.rathole ];
      };
      script = ''
        set -euo pipefail
        export PATH="${pkgs.rathole}/bin:${pkgs.dasel}/bin:${pkgs.coreutils}/bin:$PATH"

        cat ${conf} | \
          dasel put -r toml server.transport.noise.local_private_key -v "$(cat /run/agenix/rathole-noise-private-key)" | \
          dasel put -r toml server.services.moonrock-grafana.token -v "$(cat /run/agenix/rathole-moonrock-grafana-token)" | \
          dasel put -r toml server.services.moonrock-nginx-plain.token -v "$(cat /run/agenix/rathole-moonrock-nginx-plain-token)" | \
          dasel put -r toml server.services.moonrock-nginx-tls.token -v "$(cat /run/agenix/rathole-moonrock-nginx-tls-token)" > /var/lib/rathole/server.toml

        rathole /var/lib/rathole/server.toml
      '';
      serviceConfig = {
        User = "rathole";
        Group = "rathole";
        Restart = "always";
        BindReadOnlyPaths = [
          "/run/agenix:/run/agenix"
          "/run/agenix.d:/run/agenix.d"
        ];
        BindPaths = [
          "/var/lib/rathole:/var/lib/rathole"
        ];
        CapabilityBoundingSet = "cap_net_bind_service";
        AmbientCapabilities = "cap_net_bind_service";
        # Implied true by confinement.enable, but it must be false for ambient
        # capabilties to work.
        PrivateUsers = false;
      };
    };
  systemd.services.berlin-scraper = {
    enable = true;
    after = [ "basic.target" ];

    serviceConfig = {
      User = "root";
      Restart = "always";
      ExecStartPre="-${pkgs.coreutils}/bin/mkdir /var/lib/berlin-scraper";
      ExecStart = "${pkgs.berlin-scraper}/bin/berlin-scraper --dbFile /var/lib/berlin-scraper/wbs.sqlite";
      EnvironmentFile = "/run/agenix/berlin-scraper-telegram-bot-token";
    };
  };
}
