{ pkgs, ... }: {
  imports = [
    ./hardware-configuration.nix
    ./networking.nix # generated at runtime by nixos-infect
    ./gatekeeper.nix
  ];

  boot.tmp.cleanOnBoot = true;
  zramSwap.enable = true;
  networking.hostName = "gatekeeper";
  networking.domain = "";
  services.openssh.enable = true;
  users.users.root.openssh.authorizedKeys.keys = [
    (builtins.readFile ../../public-keys/axeman)
  ];


  age.secrets = {
    berlin-scraper-telegram-bot-token = {
      file = ../../age/berlin-scraper-telegram-bot-token.age;
      mode = "0400";
      owner = "root";
      group = "root";
    };
  };
}
