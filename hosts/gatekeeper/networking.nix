{ lib, ... }: {
  # This file was populated at runtime with the networking
  # details gathered from the active system.
  networking = {
    nameservers = [ "8.8.8.8" ];
    defaultGateway = "172.31.1.1";
    defaultGateway6 = {
      address = "fe80::1";
      interface = "eth0";
    };
    dhcpcd.enable = false;
    usePredictableInterfaceNames = lib.mkForce false;
    interfaces = {
      eth0 = {
        ipv4.addresses = [
          { address="91.107.214.15"; prefixLength=32; }
        ];
        ipv6.addresses = [
          { address="2a01:4f8:c17:d26f::1"; prefixLength=64; }
          { address="fe80::9400:2ff:fe49:9ee0"; prefixLength=64; }
        ];
        ipv4.routes = [ { address = "172.31.1.1"; prefixLength = 32; } ];
        ipv6.routes = [ { address = "fe80::1"; prefixLength = 128; } ];
      };
    };
  };
  services.udev.extraRules = ''
    ATTR{address}=="96:00:02:49:9e:e0", NAME="eth0"
  '';

  networking.firewall.allowedTCPPorts = [
    22  # SSH
    80
    443
    2333 # rathole
    3000 # Grafana
  ];
  networking.firewall.allowedUDPPorts = [
    1194 # OpenVPN
  ];
}
