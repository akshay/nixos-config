{ pkgs, ...}:
{
  users.users.rathole = {
    description = "User to run rathole";
    group = "rathole";
    isSystemUser = true;
    home = "/var/lib/rathole";
    createHome = true;
  };

  users.groups.rathole = {};

  age.secrets = {
    rathole-moonrock-grafana-token = {
      file = ../../age/rathole-moonrock-grafana-token.age;
      mode = "0400";
      owner = "rathole";
      group = "rathole";
    };

    rathole-noise-private-key = {
      file = ../../age/rathole-moonrock-noise-private-key.age;
      mode = "0400";
      owner = "rathole";
      group = "rathole";
    };

    rathole-moonrock-nginx-plain-token = {
      file = ../../age/rathole-moonrock-nginx-plain-token.age;
      mode = "0400";
      owner = "rathole";
      group = "rathole";
    };

    rathole-moonrock-nginx-tls-token = {
      file = ../../age/rathole-moonrock-nginx-tls-token.age;
      mode = "0400";
      owner = "rathole";
      group = "rathole";
    };
  };

  systemd.services.rathole =
    let
      conf = pkgs.writeText "rathole-client.toml" (builtins.readFile ./rathole-config/client.toml);
    in {
      enable = true;
      description = "Rathole - Reverse proxy for NAT traversal";
      wantedBy = [ "basic.target" ];
      # confinement = {
      #   enable = true;
      #   packages = [ pkgs.rathole ];
      # };
      script = ''
        set -euo pipefail
        export PATH="${pkgs.rathole}/bin:${pkgs.dasel}/bin:${pkgs.coreutils}/bin:$PATH"

        cat ${conf} | \
          dasel put -r toml client.transport.noise.local_private_key -v "$(cat /run/agenix/rathole-noise-private-key)" | \
          dasel put -r toml client.services.moonrock-grafana.token -v "$(cat /run/agenix/rathole-moonrock-grafana-token)" | \
          dasel put -r toml client.services.moonrock-nginx-plain.token -v "$(cat /run/agenix/rathole-moonrock-nginx-plain-token)" | \
          dasel put -r toml client.services.moonrock-nginx-tls.token -v "$(cat /run/agenix/rathole-moonrock-nginx-tls-token)" > /var/lib/rathole/client.toml

        rathole /var/lib/rathole/client.toml
      '';
      serviceConfig = {
        User = "rathole";
        Group = "rathole";
        Restart = "always";
        # BindReadOnlyPaths = [
        #   "/run/agenix:/run/agenix"
        #   "/run/agenix.d:/run/agenix.d"
        #   # "/etc/hots:/etc/hosts"
        #   # "/etc/resolv.conf:/etc/resolv.conf"
        # ];
        # BindPaths = [
        #   "/var/lib/rathole:/var/lib/rathole"
        # ];
      };
    };
}
