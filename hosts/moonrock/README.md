# Moonrock

## Runs

* Nextcloud
* Grafana
* Rathole (client)

## Disk Encryption

The disks are encrypted using ZFS. They cannot be decrypted in stage 1 of boot
because systemd-boot and grub are not supported and without that we cannot have
initrd secrets.

The two disks are part of one zpool called `stash`. There is 1 dataset inside
`stash`, its called `encrypted`. There are further 2 datasets inside
`encrypted`, they are called `postgresql` and `nextcloud`.

The unit which loads key for `encrypted` is called `zfs-import-stash.service`.
This unit uses `systemd-ask-password` to load the key. To provide the key, the
operator must run this command `systemd-tty-ask-password-agent`. **NOTE:** The
unit doesn't like it if the key was already loaded, so in case the key is
already loaded it has to be unloaded using `zfs unload-key -a` and the unit has
to be restarted.

Once the key is loaded, the datasets need to be mounted manually like this:
```
zfs mount stash/encrypted
zfs mount stash/encrypted/postgresql
zfs mount stash/encrypted/nextcloud
```
