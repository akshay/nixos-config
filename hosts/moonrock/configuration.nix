{pkgs, modulesPath, config, lib, ...}:
let
  ip = "192.168.178.37";
in {
  imports = [
    ./hardware-configuration.nix
    ./rathole.nix
  ];

  nixpkgs.hostPlatform.system = "aarch64-linux";

  age.secrets = {
    nextcloud-admin-pass = {
      file = ../../age/nextcloud-admin-pass.age;
      mode = "0400";
      owner = "nextcloud";
      group = "nextcloud";
    };
  };

  boot = {
    loader.systemd-boot.enable = true;
    loader.efi.canTouchEfiVariables = false;
    supportedFilesystems = [ "zfs" ];
    zfs = {
      forceImportRoot = false;
      extraPools = [ "stash" ];
    };
  };
  services.zfs.autoScrub.enable = true;
  systemd.services.zfs-import-stash = {
    after = lib.mkForce [
      "systemd-udev-settle.service"
      "systemd-modules-load.service"
      "systemd-ask-password-console.service"
      "network.target"
    ];
    before = lib.mkForce [];
    wantedBy = lib.mkForce [ "multi-user.target" ];
  };
  networking.hostId = "03bec96d";

  networking.hostName = "moonrock";

  users.users.root.openssh.authorizedKeys.keys = [
    (builtins.readFile ../../public-keys/axeman)
    (builtins.readFile ../../public-keys/morino)
  ];

  users.users.axeman = {
    isNormalUser = true;
    extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keys = [
      (builtins.readFile ../../public-keys/axeman)
    ];
  };

  environment.systemPackages = with pkgs; [
    dig
    jq
    curl
    wget
    htop
    pciutils
    usbutils
    rclone
    tmux
  ];

  programs = {
    neovim = {
      enable = true;
      defaultEditor = true;
      vimAlias = true;
      viAlias = true;
      configure = {
        packages.myVimPackages = with pkgs.vimPlugins; {
          start = [vim-nix vim-surround rainbow];
        };
      };
    };

    git.enable = true;
  };

  services = {
    openssh.enable = true;

    nextcloud = {
      enable = true;
      # home = "/data/encrypted/nextcloud";
      appstoreEnable = false;
      package = pkgs.nextcloud29;
      config = {
        adminpassFile = "/run/agenix/nextcloud-admin-pass";
        adminuser = "Admin";
        dbtype = "pgsql";
      };
      settings.overwriteProtocol = "https";
      database.createLocally = true;
      datadir = "/data/encrypted/nextcloud";
      # notify_push = {
      #   enable = true;
      # };
      configureRedis = true;
      hostName = "test.bytes.gdn";
      # extraApps = {};
      https = true;
      # phpExtraExtensions = all: [ all.php-systemd ];
      # logType = "systemd";
    };

    nginx.virtualHosts.${config.services.nextcloud.hostName} = {
      forceSSL = true;
      useACMEHost = "test.bytes.gdn";
    };

    postgresql = {
      dataDir = "/data/encrypted/postgresql/${config.services.postgresql.package.psqlSchema}";
    };

    grafana = {
      enable = true;
      settings.server = {
        # Listening Address
        http_addr = ip;
        # and Port
        http_port = 3000;
        # Grafana needs to know on which domain and URL it's running
        domain = ip;
        root_url = "http://${ip}";
      };

      provision = {
        datasources.settings = {
          apiVersion = 1;
          datasources = [{
            name = "Prometheus";
            type = "prometheus";
            access = "proxy";
            url = "http://localhost:9090";
          }];
        };

        dashboards.settings = {
          apiVersion = 1;
          providers = [{
            # https://grafana.com/api/dashboards/1860/revisions/31/download
            name = "node-exporter-full";
            options.path = "${pkgs.grafana-dashboards.node-exporter-full}/share/dashboard.json";
          }];
        };
      };
    };

    prometheus = {
      enable = true;
      exporters = {
        node = {
          enable = true;
          enabledCollectors = [ "systemd" ];
        };
        postgres.enable = true;
        # nextcloud.enable = true;
        # nginx.enable = true;
        zfs.enable = true;
      };
      scrapeConfigs = [{
        job_name = "node";
        static_configs = [{
          targets = ["127.0.0.1:${toString config.services.prometheus.exporters.node.port}"];
        }];
      }];
    };
  };

  security.acme.acceptTerms = true;
  security.acme.certs = {
    "test.bytes.gdn" = {
      webroot = "/var/lib/acme/acme-challenge/";
      email = "itsakshaymankar+acme@gmail.com";
      server = "https://acme-staging-v02.api.letsencrypt.org/directory";
      group = "nginx";
    };
  };

  networking.firewall.allowedTCPPorts = [
    22  # SSH
    80
    443
    3000 # Grafana
  ];

  systemd.services.ats = {
    enable = true;
    description = "Active Thermal Service (Fan Control)";

    wantedBy = [ "basic.target" ];
    after = [ "local-fs.target" ];

    startLimitIntervalSec = 0;

    serviceConfig = {
      # Maybe this can be run as some other user.
      User = "root";
      Restart = "always";
      RestartSec = 8;
      ExecStart = "${pkgs.lua-ats}/bin/ats";
    };
  };

  # DO NOT CHANGE, unless you've read this:
  # https://nixos.org/manual/nixos/stable/options.html#opt-system.stateVersionhttps://nixos.org/manual/nixos/stable/options.html#opt-system.stateVersion
  system.stateVersion = "22.11";
}
